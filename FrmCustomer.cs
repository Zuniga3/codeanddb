﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace codeAndDataBase
{
    public partial class FrmCustomer : Form
    {
        private DataRow drcustomer;
        //private DataTable dtCustomer;
        private BindingSource bsCustomer;

        public DataRow Drcustomer {
            set
            {
                drcustomer = value;
                txt_dni.Text = drcustomer["dni"].ToString();
                txt_nombre.Text = drcustomer["nombre"].ToString();
                txt_edad.Text = drcustomer["edad"].ToString();
            }
        }

        public FrmCustomer()
        {
            InitializeComponent();
            bsCustomer = new BindingSource();
        }

        private void FrmCustomer_Load(object sender, EventArgs e)
        {
            //dtCustomer = dsCoustomer.Cliente;
            bsCustomer.DataSource = dsCoustomer;
            bsCustomer.DataMember = dsCoustomer.Tables["Cliente"].TableName;
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            string nombre; int edad;

            nombre = txt_nombre.Text;
            edad = int.Parse(txt_edad.Text);
            

            if (drcustomer != null)
            {
                int originalDNI = int.Parse(drcustomer["dni"].ToString());
                string OriginalNAME = drcustomer["nombre"].ToString();
                int originalAGE = int.Parse(drcustomer["edad"].ToString());

                /*DataRow QueryRow = dsCoustomer.Cliente.NewRow();
                QueryRow["dni"] = txt_dni.Text;
                QueryRow["nombre"] = nombre;
                QueryRow["edad"] = edad;*/

                this.customerTableAdapter.Update(nombre, edad, originalDNI,
                    OriginalNAME, originalAGE, originalDNI);
            }
            else
            {
                this.customerTableAdapter.Insert(nombre, edad);
            }
            Dispose();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
