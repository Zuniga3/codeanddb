﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace codeAndDataBase
{
    public partial class FrmCustomerDetails : Form
    {
        private BindingSource bscustomer;

        public FrmCustomerDetails()
        {
            InitializeComponent();
            bscustomer = new BindingSource();
        }


        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void LblSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bscustomer.Filter = string.Format("dni like '*{0}*' or nombre like '*{0}*' or edad like '*{0}*'", lblSearch.Text);
            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void FrmCustomerDetails_Load(object sender, EventArgs e)
        {
            this.customerTableAdapter.Fill(this.dsCustomer.Cliente);
            bscustomer.DataSource = dsCustomer;
            bscustomer.DataMember = dsCustomer.Tables["Cliente"].TableName;
            dgvCustomer.DataSource = bscustomer;
            dgvCustomer.AutoGenerateColumns = true;
        }

        private void BtnNew_Click(object sender, EventArgs e)
        {
            FrmCustomer fc = new FrmCustomer();
            fc.ShowDialog();
            this.customerTableAdapter.Fill(this.dsCustomer.Cliente);
        }

        private void Btn_Delate_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvCustomer.SelectedRows;
            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;
            DialogResult result = MessageBox.Show(this, "Realmente desea eliminar ese registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            int dni = int.Parse(drow["dni"].ToString());
            string nombre = drow["nombre"].ToString();
            int edad = int.Parse(drow["edad"].ToString());

            if (result == DialogResult.Yes)
            {
                this.customerTableAdapter.Delete(dni, nombre, edad);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.customerTableAdapter.Fill(this.dsCustomer.Cliente);
            }
        }

        private void Btn_edit_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvCustomer.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;
            FrmCustomer fc = new FrmCustomer();
            fc.Drcustomer = drow;
            fc.ShowDialog();
            this.customerTableAdapter.Fill(this.dsCustomer.Cliente);
        }
    }
}
